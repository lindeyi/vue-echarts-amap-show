import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/views/main/index.vue';
import Login from '@/views/login/index.vue';
import Common from '@/views/main/common.vue';
import Stat from '@/views/statistics/index.vue';
import Xdry from '@/views/statistics/xdry.vue';
import Buy from '@/views/warning/buy.vue';
import Warn from '@/views/warning/index.vue';
import Fore from '@/views/forecast/index.vue';
import Ysdwp from '@/views/statistics/ysdwp.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Index
    }, {
      path: '/login',
      name: 'login',
      component: Login
    }, {
      path: '/video',
      name: 'video',
      component: (resolve) => require(['@/components/video.vue'], resolve)
    }, {
      path: '/test',
      name: 'test',
      component: (resolve) => require(['@/views/test/test.vue'], resolve)
    }, {
      path: '/common',
      name: 'common',
      component: (resolve) => require(['@/views/test/common.vue'], resolve)
    }, {
      path: '/common0',
      name: 'common0',
      component: (resolve) => require(['@/views/test/common0.vue'], resolve)
    }, {
      path: '/amap',
      name: 'amap',
      component: (resolve) => require(['@/views/test/amap.vue'], resolve)
    }, {
      path: '/amap0',
      name: 'amap0',
      component: (resolve) => require(['@/views/test/amap0.vue'], resolve)
    }, {
      path: '/map',
      name: 'map',
      component: (resolve) => require(['@/views/test/echarts-map.vue'], resolve)
    }, {
      path: '/world',
      name: '世界地图',
      component: (resolve) => require(['@/views/warning/world.vue'], resolve)
    }, {
      path: '/idcard',
      name: 'idcard',
      meta: {login: false},
      component: (resolve) => require(['@/views/test/idcard.vue'], resolve)
    }, {
      path: '/stat',
      name: '统计',
      component: Common,
      children: [
        {
          path: 'index',
          name: '吸毒人员',
          meta: {before: {name: '复吸预测', path: '/fore/index'}, after: {name: '人员预警', path: '/warn/index'}},
          component: Stat
        }, {
          path: 'xdry',
          name: '社会面分布',
          meta: {before: {name: '复吸预测', path: '/fore/index'}, after: {name: '人员预警', path: '/warn/index'}},
          component: Xdry
        }, {
          path: 'ysdwp',
          name: '易涉毒物品',
          meta: {before: {name: '药物滥用预警', path: '/warn/buy'}, after: {name: '复吸预测', path: '/fore/index'}},
          component: Ysdwp
        }
      ]
    }, {
      path: '/warn',
      name: '预警',
      component: Common,
      children: [
        {
          path: 'buy',
          name: '药物滥用预警',
          meta: {before: {name: '人员预警', path: '/warn/index'}, after: {name: '易涉毒物品', path: '/stat/ysdwp'}},
          component: Buy
        }, {
          path: 'index',
          name: '人员预警',
          meta: {before: {name: '吸毒人员', path: '/stat/index'}, after: {name: '药物滥用预警', path: '/warn/buy'}},
          component: Warn
          // component: (resolve) => require(['@/views/hn/index.vue'], resolve)
        }, {
          path: 'country/:type',
          name: '出访各类型国家',
          meta: {before: {name: '吸毒人员', path: '/stat/index'}, after: {name: '药物滥用预警', path: '/warn/buy'}},
          component: (resolve) => require(['@/views/hn/countrys.vue'], resolve)
        }, {
          path: 'table',
          name: '表格分析',
          meta: {before: {name: '吸毒人员', path: '/stat/index'}, after: {name: '药物滥用预警', path: '/warn/buy'}},
          component: (resolve) => require(['@/views/hn/table.vue'], resolve)
        }, {
          path: 'table/:type',
          name: '出访各类型国家表格分析',
          meta: {before: {name: '吸毒人员', path: '/stat/index'}, after: {name: '药物滥用预警', path: '/warn/buy'}},
          component: (resolve) => require(['@/views/hn/countrytable.vue'], resolve)
        }, {
          path: 'yearVisit',
          name: '年度出访情况',
          meta: {before: {name: '吸毒人员', path: '/stat/index'}, after: {name: '药物滥用预警', path: '/warn/buy'}},
          component: (resolve) => require(['@/views/hn/yearVisit.vue'], resolve)
        }, {
          path: 'visitTable',
          name: '出访情况表格',
          meta: {before: {name: '吸毒人员', path: '/stat/index'}, after: {name: '药物滥用预警', path: '/warn/buy'}},
          component: (resolve) => require(['@/views/hn/yearVisitTable.vue'], resolve)
        }
      ]
    }, {
      path: '/fore',
      name: '预测',
      component: Common,
      children: [
        {
          path: 'index',
          name: '复吸预测',
          meta: {before: {name: '易涉毒物品', path: '/stat/ysdwp'}, after: {name: '吸毒人员', path: '/stat/index'}},
          component: Fore
        }
      ]
    }
  ]
});
// export default new Router({
//   mode: 'history',
//   routes: [
//     {
//       path: '/',
//       component: (resolve) => require(['@/views/main/index.vue'], resolve)
//     }, {
//       path: '/login',
//       name: 'login',
//       component: (resolve) => require(['@/views/login/index.vue'], resolve)
//     }, {
//       path: '/video',
//       name: 'video',
//       component: (resolve) => require(['@/components/video.vue'], resolve)
//     }, {
//       path: '/test',
//       name: 'test',
//       component: (resolve) => require(['@/views/test/test.vue'], resolve)
//     }, {
//       path: '/common',
//       name: 'common',
//       component: (resolve) => require(['@/views/test/common.vue'], resolve)
//     }, {
//       path: '/common0',
//       name: 'common0',
//       component: (resolve) => require(['@/views/test/common0.vue'], resolve)
//     }, {
//       path: '/amap',
//       name: 'amap',
//       component: (resolve) => require(['@/views/test/amap.vue'], resolve)
//     }, {
//       path: '/amap0',
//       name: 'amap0',
//       component: (resolve) => require(['@/views/test/amap0.vue'], resolve)
//     }, {
//       path: '/map',
//       name: 'map',
//       component: (resolve) => require(['@/views/test/echarts-map.vue'], resolve)
//     }, {
//       path: '/stat',
//       name: '统计',
//       component: (resolve) => require(['@/views/main/common.vue'], resolve),
//       children: [
//         {
//           path: 'index',
//           name: '万人吸毒率',
//           meta: {before: {name: '复吸预测', path: '/fore/index'}, after: {name: '吸毒人员', path: '/stat/xdry'}},
//           component: (resolve) => require(['@/views/statistics/index.vue'], resolve)
//         }, {
//           path: 'xdry',
//           name: '吸毒人员',
//           meta: {before: {name: '戒毒/康复人员', path: '/stat/index'}, after: {name: '购药预警', path: '/warn/buy'}},
//           component: (resolve) => require(['@/views/statistics/xdry.vue'], resolve)
//         }
//       ]
//     }, {
//       path: '/warn',
//       name: '预警',
//       component: (resolve) => require(['@/views/main/common.vue'], resolve),
//       children: [
//         {
//           path: 'buy',
//           name: '购药预警',
//           meta: {before: {name: '吸毒人员', path: '/stat/xdry'}, after: {name: '人员预警', path: '/warn/index'}},
//           component: (resolve) => require(['@/views/warning/buy.vue'], resolve)
//         }, {
//           path: 'index',
//           name: '人员预警',
//           meta: {before: {name: '购药预警', path: '/warn/buy'}, after: {name: '复吸预测', path: '/fore/index'}},
//           component: (resolve) => require(['@/views/warning/index.vue'], resolve)
//         }
//       ]
//     }, {
//       path: '/fore',
//       name: '预测',
//       component: (resolve) => require(['@/views/main/common.vue'], resolve),
//       children: [
//         {
//           path: 'index',
//           name: '复吸预测',
//           meta: {before: {name: '人员预警', path: '/warn/index'}, after: {name: '戒毒/康复人员', path: '/stat/index'}},
//           component: (resolve) => require(['@/views/forecast/index.vue'], resolve)
//         }
//       ]
//     }
//   ]
// });
