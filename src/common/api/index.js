import {get, post} from '@/common/js/axios';

/** 天气 */
export const getWeather = params => get('/test/waether', params);

/** 万人吸毒率 */
export const fetchStatDrugRate = params => get('/test/stat/drug/rate', params);

/** 吸毒人员 */
export const fetchStatDrugNum = params => get('/test/stat/drug/num', params);

/** 查询人员 */
export const fetchPersons = params => get('/test/fore/drug/person', params);

/** 复吸预测 */
export const fetchDrugAgain = params => get('/test/fore/drug/again', params);

/** 人员集聚 */
export const fetchWarnGather = params => get('/test/warn/gather', params);

/** 购药预警 */
export const fetchWarnBuy = params => get('/test/warn/buy', params);

/** 获取各区信息 */
export const fetchDistrictInfo = params => get('/test/district/info', params);

/** 登录 */
export const login = params => post('/test/login', params);
