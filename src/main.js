// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import echarts from 'echarts';
import VueAMap from 'vue-amap';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import store from './vuex/store';
import vueg from 'vueg';
import 'vueg/css/transition-min.css';

Vue.prototype.$echarts = echarts;
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(VueAMap);
Vue.use(vueg, router, {
  duration: '.6',
  firstEntryDisable: true,
  forwardAnim: 'fadeInUp',
  backAnim: 'fadeInDown',
  shadow: true
});

router.beforeEach(function (to, from, next) {
  if (to.path === '/login') {
    next();
  } else {
    console.log(to);
    if ('login' in to.meta && to.meta.login === false) {
      next();
      return;
    }
    if (!sessionStorage.getItem('user')) {
      if (to.path === '/') {
        next('/login');
      } else {
        next({name: 'login', params: {path: to.path}}); // 登录跳转页面
      }
    } else {
      next();
    }
  }
});
VueAMap.initAMapApiLoader({
  key: 'e291119044806cb68065850ff1003e68',
  plugin: ['Autocomplete', 'PlaceSearch', 'GraspRoad', 'Driving', 'Scale', 'OverView', 'ToolBar', 'MapType', 'PolyEditor', 'CircleEditor', 'Map3D', 'DistrictLayer', 'AMap.DistrictLayer'],
  uiVersion: '1.0.11', // ui库版本，不配置不加载,
  v: '1.4.10'
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
